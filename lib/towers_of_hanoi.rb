# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.
require 'byebug'
class TowersOfHanoi
  attr_reader :towers

  def initialize
    @towers = [[3, 2, 1], [], []]
  end

  def play
    puts render
    puts 'Please enter a move:'
    input = gets.chomp.split(' ').map(&:to_i)
    from_tower, to_tower = input
    move(from_tower, to_tower)
  end

  def move(from_tower, to_tower)
    unless valid_move?(from_tower, to_tower)
      puts 'Invalid move try again.'
      play
    end
    towers[to_tower] << towers[from_tower].pop
    if won?
      puts render
      'You win!'
    else
      play
    end
  end

  def valid_move?(from_tower, to_tower)
    if [from_tower, to_tower].none? { |n| [0, 1, 2].include?(n) }
      false
    elsif towers[from_tower].empty?
      false
    elsif towers[to_tower].empty?
      true
    else
      towers[from_tower].last < towers[to_tower].last
    end
  end

  def won?
    return true if towers[1..2].any? { |tower| tower.length === 3 }
  end

  def render
    line_three, line_two, line_one = '', '', ''

    towers.each do |(first, second, third)|
      line_three += first ? ('0' * first) + (' ' * (4 - first)) : '    '
      line_two += second ? ('0' * second) + (' ' * (4 - second)) : '    '
      line_one += third ? ('0' * third) + (' ' * (4 - third)) : '    '
    end

    "#{line_one}\n#{line_two}\n#{line_three}"
  end

  if $PROGRAM_NAME == __FILE__
    t = TowersOfHanoi.new
    puts t.play
  end

end
